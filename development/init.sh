#!/bin/sh

if [ ! -d "/data/geth" ]; then
  # initialize datadir with genesis.json file
  geth --datadir=/data init /genesis.json

  # copy list of static nodes (bootnode, validator-1, validator-2, validator-3)
  cp /config.toml /data/geth/config.toml

  # copy private node key
  echo $NODEKEY > /data/geth/nodekey

  # copy keystore password
  echo $PASSWORD > /data/password.txt
fi
