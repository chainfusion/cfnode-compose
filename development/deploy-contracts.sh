#!/bin/sh

wait_up() {
  echo "Waiting HTTP RPC and explorer to launch..."
  while ! nc -z localhost 8545 > /dev/null; do
    sleep 0.2
  done
  while ! curl -f -s http://localhost:4000/api/v1/health > /dev/null; do
    sleep 0.2
  done
  echo "HTTP RPC and explorer was launched"
}

set -e

SYSTEM_CONTRACTS_DIR="../../chainfusion-contracts"
ERC20_CONTRACTS_DIR="../../erc-20-bridge-contracts"

declare -a chains=("bg1" "bg2" "bg3")

[ ! -d $SYSTEM_CONTRACTS_DIR ] && echo "Directory $SYSTEM_CONTRACTS_DIR does not exists." && exit 1
[ ! -d $ERC20_CONTRACTS_DIR ] && echo "Directory $ERC20_CONTRACTS_DIR does not exists." && exit 1

# Remove previous configs

rm -f $SYSTEM_CONTRACTS_DIR/contracts*.json
rm -f $SYSTEM_CONTRACTS_DIR/config*.json
rm -f $ERC20_CONTRACTS_DIR/contracts*.json
rm -f $ERC20_CONTRACTS_DIR/config*.json

# Deploy system contracts

wait_up
pushd $SYSTEM_CONTRACTS_DIR
npx hardhat --network localhost run scripts/deploy-chainfusion.ts

for chain in "${chains[@]}"
do
    npx hardhat --network $chain run scripts/deploy-chain.ts
done

npx hardhat run scripts/generate-bridge-config.ts
popd

# Copy configs

cp $SYSTEM_CONTRACTS_DIR/contracts*.json $ERC20_CONTRACTS_DIR
cp $SYSTEM_CONTRACTS_DIR/bridgeconfig.yaml bridgeconfig-1.yaml
cp $SYSTEM_CONTRACTS_DIR/bridgeconfig.yaml bridgeconfig-2.yaml
cp $SYSTEM_CONTRACTS_DIR/bridgeconfig.yaml bridgeconfig-3.yaml
cp $SYSTEM_CONTRACTS_DIR/bridgeconfig.yaml bridgeconfig-4.yaml

# Deploy ERC-20 contracts

wait_up
pushd $ERC20_CONTRACTS_DIR
npx hardhat --network localhost run scripts/deploy-chainfusion.ts

for chain in "${chains[@]}"
do
    npx hardhat --network $chain run scripts/deploy-chain.ts
    npx hardhat --network $chain run scripts/deploy-token.ts
done

npx hardhat run scripts/generate-dapp-config.ts
popd
