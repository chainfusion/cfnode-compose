# ChainFusion cfnode-compose

This repository used by deploying contracts and starting validators work

## Development

### Start cfnode

Use next command in cfnode repository

```
$ docker build --build-arg GIT_USERNAME=$GIT_USERNAME --build-arg GIT_PASSWORD=$GIT_PASSWORD -t cfnode .
```

### Start all containers

```
$ make up
```

### Deploy contracts

```
$ make deploy
```

### View logs

```
$ make logs
```

### Restart node

For restart firstly use next command

```
$ make wipe
```

And after delete contracts.json and contracts-*.json in chainfusion contract and ers 20 bridge contract repositories 